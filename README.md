# Persona Elixir

> A Persona client library for Elixir.

## Installation

The package can be installed by adding `persona_elixir` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:persona_elixir, github: "arkoar-group/persona_elixir"}
  ]
end
```

## Usage

```elixir
client = PersonaElixir.Client.new(%{
  protocol: "https",
  ip: "127.0.0.1",
  port: 4102,
  nethash: "b4e87739ca85f7eabf844a643930573e9a2dd9da291662e74d26962b5c3f0ed9",
  version: "0.0.1"
})

# or

client = PersonaElixir.Client.new(%{
  url: "https://127.0.0.1:4102",
  nethash: "b4e87739ca85f7eabf844a643930573e9a2dd9da291662e74d26962b5c3f0ed9",
  version: "0.0.1",
  network_address: PersonaElixir.Client.mainnet_network_address
})

{:ok, response} = PersonaElixir.Transaction.create(
  client,
  PersonaElixir.Client.mainnet_network_address,
  "recipientId",
  "amount",
  "vendorField",
  "secret",
  "secondSecret"
)

IO.puts response["transactionIds"]
```

# To-Do

- Add Responses to Docs

## Testing

``` bash
$ mix test
```

## Security

If you discover a security vulnerability within this package, please send an e-mail to hello@brianfaust.me. All security vulnerabilities will be promptly addressed.

## Credits

- [Arkoar Group](https://github.com/arkoar-group)
- [All Contributors](../../contributors)

## License

[MIT](LICENSE) © [Brian Faust](https://brianfaust.me)
