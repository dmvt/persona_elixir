defmodule PersonaElixir.Vote do
  @moduledoc """
  Documentation for PersonaElixir.Vote.
  """

  import PersonaElixir

  @doc """
  Sign and create a new vote.

  ## Examples

      iex> PersonaElixir.Vote.vote(client)
      :world

  """
  @spec vote(PersonaElixir.Client, String.t(), String.t(), String.t()) :: PersonaElixir.response()
  def vote(client, secret, delegate, second_secret \\ nil) do
    transaction =
      PersonaElixir.Util.TransactionBuilder.create_vote(
        ["+" <> delegate],
        secret,
        second_secret,
        client.network_address
      ) |> PersonaElixir.Util.TransactionBuilder.transaction_to_params

    post(client, 'peer/transactions', %{transactions: [transaction]})
  end

  @doc """
  Sign and create a new unvote.

  ## Examples

      iex> PersonaElixir.Vote.unvote(client)
      :world

  """
  @spec unvote(PersonaElixir.Client, String.t(), String.t(), String.t()) :: PersonaElixir.response()
  def unvote(client, secret, delegate, second_secret \\ nil) do
    transaction =
      PersonaElixir.Util.TransactionBuilder.create_vote(
        ["-" <> delegate],
        secret,
        second_secret,
        client.network_address
      ) |> PersonaElixir.Util.TransactionBuilder.transaction_to_params

    post(client, 'peer/transactions', %{transactions: [transaction]})
  end
end
